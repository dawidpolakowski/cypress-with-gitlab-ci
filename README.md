# Cypress with GitLab CI

Cypress with GitLab CI -  in this project we are building a working example of Cypress test suite integrated with GitLab CI 
We are using cypress-cucumber-preprocessor to work with feature files syntax.

## Cypress cypress-cucumber-preprocessor working example

## Install

To install project simply clon it and run 

```
npm i --save-dev 
```

## Open Cypress

To open Cypress 
```
npx cypress open
```
## CI

At the moment CI will be triggered on every commit.
 I will update rules and prepare a short tutorial to help you in learning Cypress. 

 Thanks
 Dawid
