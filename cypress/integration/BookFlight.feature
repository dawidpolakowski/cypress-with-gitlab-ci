Feature: Phptravels FLIGHTS Page

    I want to make a flight booking on phptravels

@focus   
  Scenario: Make a flight booking on phptravels
    Given I open "https://www.phptravels.net" page
    When I navigate to "flights" page
    And I select a flight from "London" to "Madrid" in future
    And I select multiple passengers and press search
    And I select an first flight
    Then I fill Personal Information and confirm booking
    And I checking the details and proceed to the Payment
