import { should } from 'chai'
import flightPage from './../PageObjects/FlightPage.js'

And('I select a flight from {string} to {string} in future', (from, destination) => {

    flightPage.pickFrom(from)
    flightPage.pickDestination(destination)
})
And('I select multiple passengers and press search', () => {

    flightPage.clickPassengers()
    //    first variable 1 Adults 2 Childs 3 Infants x times to click
    flightPage.addPassengers(1, 2)
    flightPage.addPassengers(2, 2)
    flightPage.addPassengers(3, 1)
    flightPage.clickPassengers()
    cy.get('#flights-search')
        .click({ force: true })
})
And('I select an first flight', () => {

    cy.get('section > ul > li:nth-child(1)')
        .findAllByText('Book Now')
        .should('exist')
        .click({ force: true })
})
When('I fill Personal Information and confirm booking', () => {

    cy.get('div.gateway_bank-transfer')
        .click()
    cy.get('#booking')
        .click()
})
Then(`I checking the details and proceed to the Payment`, () => {

    cy.get('div.payment-received-list > div:nth-child(2)').should(($div) => {
        expect($div).to.contain('First Name: John')
    })
    flightPage.proceedToPayment() 
cy.wait(5000)
    cy.get('.aut-iframe')
    
    .then(function($ele){
        let ifele = $ele.contains().find('.card-body')
        cy.wrap(ifele).contain('Pay With Bank Transfer')
    })    
    
    })
       