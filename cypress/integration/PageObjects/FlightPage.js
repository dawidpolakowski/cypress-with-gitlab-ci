/// <reference types="cypress" />
import flightData from './../../fixtures/flightPage.json'

class FlightPage {

    cookieStop() {

        cy.get('#cookie_stop')
            .click()
    }
    pickFrom(from) {

        cy.get('#autocomplete')
            .should('be.visible')
            .click({ force: true })
            .type(from)
        cy.findAllByText(flightData.fromAirport)
            .should('exist')
            .click({ force: true })
    }
    pickDestination(destination) {

        cy.get('#autocomplete2')
            .should('be.visible')
            .click({ force: true })
            .type(destination)
        cy.findAllByText(flightData.destinationAirport)
            .should('exist')
            .click({ force: true })
    }

    pickDepartureDate() {

        cy.get('#departure')
            .should('be.visible')
            .click({ force: true })
        cy.get('body > div:nth-child(23) > div.datepicker-days > table > thead > tr:nth-child(1) > th.next > i')
            .click({ force: true })
        cy.get('body > div:nth-child(23) > div.datepicker-days > table > tbody > tr:nth-child(2) > td:nth-child(2)')
            .click({ force: true })
    }
    clickPassengers() {
        cy.get('#onereturn > div.col-lg-1.pr-0 > div > div > div > a > i')
        .click({ force: true })
    }
    addPassengers(n,j) {

        for (let i = 0; i < j; i++) {
            cy.get(`#onereturn > div.col-lg-1.pr-0 > div > div > div > div > div:nth-child(${n}) > div > div > div.qtyInc > i`)
            .click({ force: true })
    }
}
proceedToPayment() {
    cy.get('#form')
            .should('be.visible')
            .click({ force: true })
    
}

}

const flightPage = new FlightPage();

export default flightPage