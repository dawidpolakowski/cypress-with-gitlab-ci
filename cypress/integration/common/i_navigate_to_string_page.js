And(`I navigate to {string} page`, (text) => {
  cy.get('nav > ul')
  .findByText(text)
  .should('exist')
  .click()
})