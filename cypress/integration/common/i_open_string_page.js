import flightPage from './../PageObjects/FlightPage.js'

Given('I open {string} page', (pageUrl) => {
  cy.visit(pageUrl);
  flightPage.cookieStop();
})