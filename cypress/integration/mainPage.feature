Feature: Phptravels Main Page

  I want to open a phptravels homepage
  
  @focus
  Scenario: Opening a phptravels demo page
    Given I open "https://www.phptravels.net/" page
    Then I see "PHPTRAVELS" in the title